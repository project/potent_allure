<?php

/**
 * @file
 * Implements().
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * @file
 * Potent_allure theme file.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function potent_allure_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  if ($form['#attributes']['class'][0] == 'system-theme-settings') {
    $form['#attached']['library'][] = 'potent_allure/theme.setting';
    global $base_url;
    $form['potent_allure_info'] = [
      '#markup' => '<h2><br/>Advanced Theme Settings</h2><div class="messages messages--warning">Clear cache after making any changes in theme settings. <a href="../../config/development/performance">Click here to clear cache</a></div>',
    ];
    // Header Top Position.
    $form['header_top_Position'] = [
      '#type' => 'select',
      '#title' => t('Choose User Account Position'),
      '#options' => [
        'right' => t('Right'),
        'left' => t('left'),
        'center' => t('Center'),
      ],
      '#default_value' => theme_get_setting('header_top_Position', 'potent_allure'),
      '#description' => t("Select User Account Position."),
    ];

    // Slider Display Or Not Option.
    $form['slider']['slider_details'] = [
      '#type' => 'details',
      '#title' => t('Are You Want Display Slider Or Not'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['slider']['slider_details']['select_slider'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Slider'),
      '#default_value' => theme_get_setting('select_slider'),
      '#description' => t('Show/Hide'),

    ];
    $display = theme_get_setting('select_slider');

    if ($display) {
      $form['slider']['slider_details']['slide_num'] = [
        '#type' => 'number',
        '#title' => t('Select Number of Slider Display'),
        '#min' => 1,
        '#required' => TRUE,
        '#default_value' => theme_get_setting('slide_num'),
        '#description' => t("Enter Number of slider you want to display"),
      ];
      $form['slider']['slider_details']['banner_num'] = [
        '#type' => 'number',
        '#type' => 'hidden',
      ];

    }
    else {

      $form['slider']['slider_details']['slide_num'] = [
        '#type' => 'number',
        '#type' => 'hidden',
      ];
    }
    $Num_slide = theme_get_setting('slide_num');
    if ($display && $Num_slide > 0) {

      $form['dis_slider']['dis_slider_details'] = [
        '#type' => 'details',
        '#title' => t('Slider'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      ];
      $form['dis_slider']['dis_slider_details']['dis_slider_box'] = [
        '#type' => 'checkbox',
        '#default_value' => theme_get_setting('dis_slider_box', 'potent_allure'),
        '#title' => t('Show slider'),
        '#required' => TRUE,
        '#description' => t('Show/hide slider'),
      ];
      $form['dis_slider']['dis_slider_details']['slide'] = [
        '#markup' => t('You can change the  each slide Information in the following Slide Setting fieldsets.'),

      ];
      for ($i = 1; $i <= $Num_slide; $i++) {
        $form['dis_slider']['dis_slider_details']['slide' . $i] = [
          '#type' => 'fieldset',
          '#title' => t('Slide  @i', ['@i' => $i]),
          '#attributes' => [
            'id' => 'slide' . $i,
            'class' => ['slide_show'],
          ],
        ];
        $form['dis_slider']['dis_slider_details']['slide' . $i]['slide' . $i . '_url_text'] = [
          '#type' => 'textfield',
          '#title' => t('Title'),
          '#default_value' => theme_get_setting('slide' . $i . '_url_text', 'potent_allure'),

        ];
        $form['dis_slider']['dis_slider_details']['slide' . $i]['slide' . $i . '_url'] = [
          '#type' => 'textfield',
          '#title' => t('Content Url'),
          '#default_value' => theme_get_setting('slide' . $i . '_url', 'potent_allure'),
        ];
        $form['dis_slider']['dis_slider_details']['slide' . $i]['slide' . $i . '_des'] = [
          '#type' => 'textfield',
          '#title' => t('Description'),
          '#default_value' => theme_get_setting('slide' . $i . '_des', 'potent_allure'),
        ];
        $form['dis_slider']['dis_slider_details']['slide' . $i]['slide' . $i . '_date'] = [
          '#type' => 'date',
          '#title' => t('Current Date'),
          '#default_value' => theme_get_setting('slide' . $i . '_date', 'potent_allure'),
        ];
        $form['dis_slider']['dis_slider_details']['slide' . $i]['slide' . $i . '_image'] = [
          '#type' => 'managed_file',
          '#title' => t('Slider_' . $i . 'image'),
          '#default_value' => theme_get_setting('slide' . $i . '_image', 'potent_allure'),
          '#upload_location' => 'public://',
          '#upload_validators' => [
            'file_validate_extensions' => ['gif png jpg jpeg svg'],
          ],

        ];
        // Make the uploaded images permanent.
        $img_slide = theme_get_setting('slide' . $i . '_image', 'potent_allure');
        if (!empty($img_slide)) {
          $file = File::load($img_slide[0]);
          $file->setPermanent();
          $file->save();
          $file_usage = \Drupal::service('file.usage');
          $file_usage_check = $file_usage->listUsage($file);
          if (empty($file_usage_check)) {
            $file_usage->add($file, 'potent_allure', 'theme', $img_slide[0]);
          }
        }
      }
      $form['dis_banner']['dis_banner_details']['banner'] = [
        '#type' => 'hidden',

      ];

    }
    else {

      $form['dis_banner']['dis_banner_details'] = [
        '#type' => 'details',
        '#title' => t('Banner'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      ];
      $form['dis_banner']['dis_banner_details']['dis_banner_box'] = [
        '#type' => 'checkbox',
        '#default_value' => theme_get_setting('dis_banner_box', 'potent_allure'),
        '#title' => t('Show Banner'),
        '#required' => TRUE,
        '#description' => t('Show/hide Banner'),
      ];
      $form['dis_slider']['dis_slider_details']['slide'] = [
        '#type' => 'hidden',

      ];
      $form['dis_banner']['dis_banner_details']['banner'] = [
        '#markup' => t('You can change the   each banner Information in the following Banner Setting fieldsets.'),

      ];
      for ($i = 1; $i <= 3; $i++) {
        $form['dis_banner']['dis_banner_details']['banner' . $i] = [
          '#type' => 'fieldset',
          '#title' => t('Banner  @i', ['@i' => $i]),
          '#attributes' => [
            'id' => 'banner' . $i,
            'class' => ['banner_show'],
          ],
        ];
        $form['dis_banner']['dis_banner_details']['banner' . $i]['banner' . $i . '_url_text'] = [
          '#type' => 'textfield',
          '#title' => t('Title'),
          '#default_value' => theme_get_setting('banner' . $i . '_url_text', 'potent_allure'),

        ];
        $form['dis_banner']['dis_banner_details']['banner' . $i]['banner' . $i . '_url'] = [
          '#type' => 'textfield',
          '#title' => t('Content Url'),
          '#default_value' => theme_get_setting('banner' . $i . '_url', 'potent_allure'),
        ];
        $form['dis_banner']['dis_banner_details']['banner' . $i]['banner' . $i . '_des'] = [
          '#type' => 'textfield',
          '#title' => t('Description'),
          '#default_value' => theme_get_setting('banner' . $i . '_des', 'potent_allure'),
        ];
        $form['dis_banner']['dis_banner_details']['banner' . $i]['banner' . $i . '_images'] = [
          '#type' => 'managed_file',
          '#title' => t('Banner' . $i . 'images'),
          '#default_value' => theme_get_setting('banner' . $i . '_images', 'potent_allure'),
          '#upload_location' => 'public://',
          '#upload_validators' => [
            'file_validate_extensions' => ['gif png jpg jpeg svg'],
          ],

        ];
        // Make the uploaded images permanent.
        $img_slide = theme_get_setting('banner' . $i . '_images', 'potent_allure');
        if (!empty($img_slide)) {
          $file = File::load($img_slide[0]);
          $file->setPermanent();
          $file->save();
          $file_usage = \Drupal::service('file.usage');
          $file_usage_check = $file_usage->listUsage($file);
          if (empty($file_usage_check)) {
            $file_usage->add($file, 'potent_allure', 'theme', $img_slide[0]);
          }
        }
      }
    }

    // >>>>>>>>>>> Social Media Link
    $form['show_social_icon']['social_icon'] = [
      '#type' => 'details',
      '#title' => t('Social Media Link'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['show_social_icon']['social_icon']['show_social_icon'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Social Icons'),
      '#default_value' => theme_get_setting('show_social_icon'),
      '#description'   => t("Show/Hide social media links"),
    ];
    $form['show_social_icon']['social_icon']['facebook_url'] = [
      '#type' => 'textfield',
      '#title' => t('Facebook Link'),
      '#default_value' => theme_get_setting('facebook_url'),
    ];
    $form['show_social_icon']['social_icon']['twitter_url'] = [
      '#type' => 'textfield',
      '#title' => t('Twitter Link'),
      '#default_value' => theme_get_setting('twitter_url'),
    ];
    $form['show_social_icon']['social_icon']['instagram_url'] = [
      '#type' => 'textfield',
      '#title' => t('Instagram Link'),
      '#default_value' => theme_get_setting('instagram_url'),
    ];
    $form['show_social_icon']['social_icon']['linkedin_url'] = [
      '#type' => 'textfield',
      '#title' => t('Linkedin Link'),
      '#default_value' => theme_get_setting('linkedin_url'),
    ];

    // >>>>>>>>>>Footer copyright>>>>>>>>>
    $form['footer_details'] = [
      '#type' => 'details',
      '#title' => t('Copyright'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['footer_details']['footer_details'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Copyright'),
      '#default_value' => theme_get_setting('footer_details'),
      '#description'   => t("Show/Hide Copyright"),
    ];
    $form['footer_details']['footer_copyright'] = [
      '#type' => 'textarea',
      '#title' => t('Footer Copyright:'),
      '#default_value' => theme_get_setting('footer_copyright'),
      '#description' => t("Text area for Footer Copyright."),
    ];
    $form['footer_details']['footer_deve_text'] = [
      '#type' => 'textarea',
      '#title' => t('Developer info'),
      '#default_value' => theme_get_setting('footer_deve_text'),
      '#description' => t("Developer info."),
    ];
    $form['footer_details']['footer_theme_url'] = [
      '#type' => 'textarea',
      '#title' => t('Theme Info'),
      '#default_value' => theme_get_setting('footer_theme_url'),
      '#description' => t("Theme info."),
    ];
    // Footer Subscribe Newsletter Form.
    $form['footer_subscribe'] = [
      '#type' => 'details',
      '#title' => t('Subscribe Newsletter Form '),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['footer_subscribe']['footer_subscribes'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Subscribe Form'),
      '#default_value' => theme_get_setting('footer_subscribes'),
      '#description'   => t("Show/Hide Subscribe Form"),
    ];
    $form['footer_subscribe']['footer_sub_title'] = [
      '#type' => 'textarea',
      '#title' => t('Subscribe Form Title'),
      '#default_value' => theme_get_setting('footer_sub_title'),
      '#description' => t("Text area for Footer Subscribe Form Title."),
    ];
    $form['footer_subscribe']['footer_sub_des'] = [
      '#type' => 'textarea',
      '#title' => t('Subscribe Form Description'),
      '#default_value' => theme_get_setting('footer_sub_des'),
      '#description' => t("Text area for Footer Subscribe Form Description."),
    ];

    // Color Picker.
    $form['color_settings'] = [
      '#type' => 'details',
      '#title' => t('Want To Set Color In Your Theme'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['color_settings']['color_details'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Color'),
      '#default_value' => theme_get_setting('color_details'),
      '#description'   => t("Show/Hide color"),
    ];
    $form['color_settings']['header_top_bg_color'] = [
      '#type' => 'color',
      '#title' => t('Header Top Background Color'),
      '#default_value' => theme_get_setting('header_top_bg_color', 'potent_allure'),
      '#description' => t("Choose the background color for the Top header."),
    ];
    $form['color_settings']['header_top_Link_color'] = [
      '#type' => 'color',
      '#title' => t('Header Top Link Color'),
      '#default_value' => theme_get_setting('header_top_Link_color', 'potent_allure'),
      '#description' => t("Choose the color for the Top header Link Color."),
    ];

    $form['color_settings']['header_bg_color'] = [
      '#type' => 'color',
      '#title' => t('Header Background Color'),
      '#default_value' => theme_get_setting('header_bg_color', 'potent_allure'),
      '#description' => t("Choose the background color for the header."),
    ];
    $form['color_settings']['nav_color'] = [
      '#type' => 'color',
      '#title' => t('Navbar Link Color'),
      '#default_value' => theme_get_setting('nav_color', 'potent_allure'),
      '#description' => t("Choose the  color for the Navbar Link Color."),
    ];
    $form['color_settings']['nav_hover_color'] = [
      '#type' => 'color',
      '#title' => t('Navbar Hover Link Color'),
      '#default_value' => theme_get_setting('nav_hover_color', 'potent_allure'),
      '#description' => t("Choose the  color for the Navbar Hover Link Color."),
    ];
    $form['color_settings']['drop_nav_color'] = [
      '#type' => 'color',
      '#title' => t('Drop Down Navbar Link Color'),
      '#default_value' => theme_get_setting('drop_nav_color', 'potent_allure'),
      '#description' => t("Choose the  color for the Drop Down Navbar Link Color ."),
    ];
    $form['color_settings']['drop_nav_hover_color'] = [
      '#type' => 'color',
      '#title' => t('Drop Down Navbar Hover Bg  Color'),
      '#default_value' => theme_get_setting('drop_nav_hover_color', 'potent_allure'),
      '#description' => t("Choose the background color for the Drop Down Navbar Hover bg Color ."),
    ];
    $form['color_settings']['drop_nav_hover_link_color'] = [
      '#type' => 'color',
      '#title' => t('Drop Down Navbar Hover Link Color'),
      '#default_value' => theme_get_setting('drop_nav_hover_link_color', 'potent_allure'),
      '#description' => t("Choose the  color for the Drop Down Navbar Hover link Color ."),
    ];
    $form['color_settings']['drop_bg_color'] = [
      '#type' => 'color',
      '#title' => t('Drop Down Navbar Background Color'),
      '#default_value' => theme_get_setting('drop_bg_color', 'potent_allure'),
      '#description' => t("Choose the background color for the Drop Down Navbar Background Color ."),
    ];

    $form['color_settings']['slider_bg_color'] = [
      '#type' => 'color',
      '#title' => t('Slider OR Banner Background Color'),
      '#default_value' => theme_get_setting('slider_bg_color', 'potent_allure'),
      '#description' => t("Choose the background color for the Slider OR Banner."),
    ];
    $form['color_settings']['Footer_bg_color'] = [
      '#type' => 'color',
      '#title' => t('Footer Background Color'),
      '#default_value' => theme_get_setting('Footer_bg_color', 'potent_allure'),
      '#description' => t("Choose the background color for the Footer."),
    ];
    $form['color_settings']['footer_color'] = [
      '#type' => 'color',
      '#title' => t('Footer Link Color'),
      '#default_value' => theme_get_setting('footer_color', 'potent_allure'),
      '#description' => t("Choose the background color for the Footer Link Color."),
    ];
    $form['color_settings']['footer_hover_color'] = [
      '#type' => 'color',
      '#title' => t('Footer Hover Link Color'),
      '#default_value' => theme_get_setting('footer_hover_color', 'potent_allure'),
      '#description' => t("Choose the  color for the Footer Hover Link Color."),
    ];
    $form['color_settings']['footer_text_color'] = [
      '#type' => 'color',
      '#title' => t('Footer Text Color'),
      '#default_value' => theme_get_setting('footer_text_color', 'potent_allure'),
      '#description' => t("Choose the background color for the Footer Text Color."),
    ];

    $form['#submit'][] = 'potent_allure_settings_form_submit';
    $theme = \Drupal::theme()->getActiveTheme()->getName();
    $theme_file = $base_url . '/' . \Drupal::service('extension.list.theme')->getPath($theme);
    $build_info = $form_state->getBuildInfo();
    if (!in_array($theme_file, $build_info['files'])) {
      $build_info['files'][] = $theme_file;
    }
    $form_state->setBuildInfo($build_info);

  }
}
