# Potent Allure

Potent Allure Theme is a fully responsive Drupal theme designed to give your
website a clean and modern look. It offers several customizable options that
allow admins to change colors, manage header/footer links, and toggle
slider/banner display. Footer copyright field, social menu icon, responsive
layout and highly customizable. It also supports Font Awesome and is great for
any type of blog website.

For a full description of the theme, visit the
[project page](https://www.drupal.org/project/potent_allure).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/potent_allure).

## Table of contents

- Theme Features
- Requirements
- Installation
- Configuration
- Maintainers

## Theme Features
- Custom Slider
- Fully responsive layout ensuring a seamless experience on various devices.
- Customizable color options for header and footer, link hover and slider
background.
- Easily manage header and footer links via the Drupal management interface.
- Option to display a slider or banner on the homepage.

## Requirements

This theme requires no themes outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal theme.
For further information, see
[Installing Drupal Themes](https://www.drupal.org/docs/extending-drupal/installing-themes).

## Configuration

Navigate to Administration > Appearance and enable the theme.
For theme settings option, go to Appearance > Settings > potent_allure.
Available options in the theme settings:
  - Show/hide slider on home page.
  - Show/hide and change copyright text.
  - Show/hide and change social media icon.
  - Add content for navigation bar (header), footer.
  - Hide/show and change color, background color for header and footer.
  - Text, link and slide/banner.

## Maintainer
- vijay Sharma - [VijaySharma_89](https://www.drupal.org/u/vijaysharma_89)

# Supporting organizations: 
[- Smashing Infolabs Pvt. Ltd.](https://www.drupal.org/smashing-infolabs-pvt-ltd)
